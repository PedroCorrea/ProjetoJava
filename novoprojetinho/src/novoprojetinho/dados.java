package novoprojetinho;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
public class dados
{
    Connection con;
    public boolean conecta(String local, String banco, String usuario, String senha)
    {
        boolean retorno = false;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://"+local+"/"+banco,usuario,senha);
            retorno = true;
        }
        catch(ClassNotFoundException e)
        {
            System.err.println("erro de conexão:\n"+e);
        }
        catch(SQLException e)
        {
            System.err.println("erro de conexão:\n"+e);
        }
        return retorno;
    }
    public boolean insereCliente(String tabela, String cpf, String nome, String end, String tel, String login, String senha)
    {
        boolean retorno = false;
        try
        {
             PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement("insert into "+tabela+"(cpf,nome,end,tel,login,senha,livro1,livro2,livro3) values('"+cpf+"','"+nome+"','"+end+"','"+tel+"','"+login+"','"+senha+"',0,0,0);");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro INSERT: "+ex);
        }
        return retorno;
    }
    public boolean atualizaCliente(String table, String nome, String end, String tel, String login, String senha, String cpf)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement)this.con.prepareStatement("update " + table + " set nome='" + nome + "', end='" + end + "', tel='" + tel + "', login='" + login + "', senha='" + senha + "' where cpf='" + cpf + "';");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro UPDATE:"+ex);
        }
        return retorno;
    }
    public boolean insereFunc(String tabela, String cpf, String nome, String end, String tel, String login, String senha,int tipo)
    {
        boolean retorno = false;
        try
        {
             PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement("insert into "+tabela+"(cpf,nome,end,tel,login,senha,tipo) values('"+cpf+"','"+nome+"','"+end+"','"+tel+"','"+login+"','"+senha+"',"+tipo+");");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro INSERT: "+ex);
        }
        return retorno;
    }
    public boolean atualizaFunc(String table, String nome, String end, String tel, String login, String senha, int tipo, String cpf)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement)this.con.prepareStatement("update " + table + " set nome='" + nome + "', end='" + end + "', tel='" + tel + "', login='" + login + "', senha='" + senha + "', tipo=" + tipo + " where cpf=" + cpf + ";");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro UPDATE:"+ex);
        }
        return retorno;
    }
    public boolean insereLivros(String tabela, String titulo, String autor, String editora, String data, int qtde_biblioteca, int qtde_disp)
    {
        boolean retorno = false;
        try
        {
             PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement("insert into "+tabela+"(titulo_livro,autor,editora,data,qtde_biblioteca,qtde_disp,status) values('"+titulo+"','"+autor+"','"+editora+"','"+data+"',"+qtde_biblioteca+","+qtde_disp+",1);");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro INSERT: "+ex);
        }
        return retorno;
    }
    public ResultSet consulta(String consulta)
    {
        ResultSet rs = null;
        try
        {
            System.out.println(consulta);
            PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement(consulta);
            rs = stmt.executeQuery();
        }
        catch(Exception e)
        {
            System.err.println("Erro CONSULTA: "+e);
        }
        
        return rs;
    }
    
    public boolean menosLivros(int x, String id)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement)this.con.prepareStatement("update livros set qtde_disp=" + x + " where id_livro=" + id + ";");
            
            
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro UPDATE:"+ex);
        }
        return retorno;
    }
    public boolean excluiLivros(String id)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement)this.con.prepareStatement("update livros set status=0 where id_livro=" + id + ";");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro UPDATE:"+ex);
        }
        return retorno;
    }
    public boolean atualiza(int oq, int id)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement)this.con.prepareStatement("update livros set qtde_disp=" + oq + " where id_livro=" + id + ";");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro UPDATE:"+ex);
        }
        return retorno;
    }
    public boolean livroCliente(String tabela, String livro, int cel, String cpf)
    {
        boolean retorno = false;
        try
        {
            PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement("UPDATE " + tabela + " SET "+livro+"="+cel+" WHERE cpf='"+cpf+"';");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro INSERT: "+ex);
        }
        return retorno;
    }
    public boolean insereDep(String tabela, String cpf, String nome)
    {
        boolean retorno = false;
        try
        {
             PreparedStatement stmt = (PreparedStatement) this.con.prepareStatement("insert into "+tabela+"(cpf,nome) values('"+cpf+"','"+nome+"');");
            stmt.execute();
            stmt.close();
            retorno = true;
        }
        catch(SQLException ex)
        {
            retorno = false;
            System.err.println("Erro INSERT: "+ex);
        }
        return retorno;
    }
}